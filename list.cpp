#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if (tail == 0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	//TO DO!
	Node *newNode = new Node(el, head);
	if (head == NULL) {
		head = newNode;
		tail = newNode;
	}
	else {
		tail->next = newNode;
		tail = newNode;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	// TO DO!
	Node *tmp = tail;

	if (tail == head)
	{
		tail = head = 0;
	}
	else
	{

		tail->next = NULL;
	}
	delete tmp;

	
	return NULL;
}
bool List::search(char el)
{
	// TO DO! (Function to return True or False depending if a character is in the list.
	bool b1 = true;
	bool b2 = false;
	Node* cur;
	cur = head;

	while (cur != NULL) {
		if (cur->data == el) {
			return b1;
		}
		
		cur = cur->next;

	}

	
	return NULL;
}
void List::reverse()
{
	// TO DO! (Function is to reverse the order of elements in the list.
	if (head != NULL && head->next != NULL) {

		Node* tmp = head;
		Node* n = head->next;
		Node* t = NULL;
		if (n->next != NULL) {
			t = n->next;
			head->next = NULL;
		}

		while (t->next != NULL) {
			n->next = tmp;
			tmp = n;
			n = t;
			t = t->next;
		}
		n->next = tmp;
		head = t;
		t->next = n;
	}
}
void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data << endl;
	}
	
}